# README #


### What is this repository for? ###

* This is an example of a custom element type for the developer friendly page builder laygridder.com for WordPress

### How do I get set up? ###

* Put the directory "slider-for-laygridder-com" into your WordPress plugins folder
* Make sure the LayGridder plugin and CMB2 plugin are installed and activated
* Activate the plugin "Slider for laygridder.com"
* In your Gridder, find a new button "+Slider"

### How do I modify the plugin? ###

* I used Grunt for this project, if you are not familiar with Grunt please read this http://gruntjs.com/getting-started
* If you want to use Grunt as well, go to your terminal, cd into the "slider-for-laygridder-com" directory and do "npm install"
* Then do "grunt watch" and start coding